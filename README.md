BCCE301

Quality: Maintainability

These modules are used to demonstrate an example of how the same functionality can be coded in two different ways; maintainable and unmaintainable.

Comments highlight the statements that are significant to the issue of maintainability.

The so-called unmaintainable solution boasts less lines of code, but the cost and risk of maintaining the essential control-statement for more use cases is great.

In the so-called maintainable solution a method is marked as designed for change in the MasterController class. For these sort of methods, modification during development is not a count against maintainability - the use cases are simply registered in a controlled way.