package acr79_demo.maintainable;

import ___MASTER.Controller;
import ___MASTER.View;

public abstract class NamedController extends Controller {

	public NamedController(View newView) {
		super(newView);
	}

	public abstract String getName();
}