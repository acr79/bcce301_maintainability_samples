package acr79_demo.maintainable;

import ___MASTER.View;

public class UpperCaseController extends NamedController {

	public UpperCaseController(View newView) {
		super(newView);
	}

	@Override
	public String getName() {
		return "Upper Case";
	}

	@Override
	public void go(String[] args) {
		String toChange = myView.getString("Enter the phrase");
		String changed = toChange.toUpperCase();
		myView.say(changed);
	}

}
