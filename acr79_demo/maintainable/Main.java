package acr79_demo.maintainable;

import java.util.Scanner;

import __Console.ConsoleView;
import ___MASTER.Controller;
import ___MASTER.View;

public class Main {

	public static void main(String[] args) {
		Scanner theScan = new Scanner(System.in);
		View theView = new ConsoleView(theScan);
		Controller theCont = new MasterController(theView);
		theCont.go(args);
		theScan.close();
	}
}
