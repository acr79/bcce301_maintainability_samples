package acr79_demo.maintainable;

import java.util.ArrayList;
import java.util.List;

import ___MASTER.Controller;
import ___MASTER.View;

public class MasterController extends Controller {

	public MasterController(View newView) {
		super(newView);
		controllers = new ArrayList<>();
		setUpControllers();
	}

	@Override
	public void go(String[] args) {
		myView.say("Maintainable Demo");

		// Menu text based on named controllers;
		String menuPrompt = "0 - Quit\n";
		int i;
		int limit = controllers.size();
		for (i = 0; i < limit; i++)
			menuPrompt += String.format("%d - %s\n",
					i + 1, controllers.get(i).getName()
			);

		boolean current = true;
		while (current) {
			int choice = myView.getInt(menuPrompt);
			if (choice == 0)
				current = !isQuitting();
			
			// All named controllers are up for case selection
			else if (0 < choice && choice <= limit)
				controllers.get(choice - 1).go(args);
		}

		myView.say("Demo End");

	}

	protected boolean isQuitting() {
		int x = myView.getInt("Enter 1 to quit");
		return x == 1;
	}

	protected List<NamedController> controllers;

	// Designed to be changed; focused on registration
	protected void setUpControllers() {
		controllers.add(new UpperCaseController(myView));
		controllers.add(new StarController(myView));
	}

}