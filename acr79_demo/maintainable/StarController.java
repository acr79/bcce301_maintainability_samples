package acr79_demo.maintainable;

import ___MASTER.View;

public class StarController extends NamedController {

	public StarController(View newView) {
		super(newView);
	}

	@Override
	public String getName() {
		return "Star Controller";
	}

	@Override
	public void go(String[] args) {
		int w = myView.getInt("Enter the width");
		int h = myView.getInt("Enter the height");
		String toShow = "";
		int i;
		for (i = 0; i < w; i++)
			toShow += "*";
		for (i = 0; i < h; i++)
			myView.say(toShow);
	}

}
