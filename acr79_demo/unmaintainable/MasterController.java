package acr79_demo.unmaintainable;

import ___MASTER.Controller;
import ___MASTER.View;

public class MasterController extends Controller {

	public MasterController(View newView) {
		super(newView);
	}

	@Override
	public void go(String[] args) {
		myView.say("Unmaintainable Demo");
		boolean current = true;
		while (current) {
			
			// Maintenance: Need to update menu text
			int choice = myView.getInt("0 - Quit\n1 - Upper Case\n2 - Star Controller\n");
			
			// Maintenance: Need to include conditional check
			if (choice == 0) {
				int x = myView.getInt("Enter 1 to quit");
				current = x != 1;
			} else if (choice == 1) {
				String toChange = myView.getString("Enter the phrase");
				String changed = toChange.toUpperCase();
				myView.say(changed);
			} else if (choice == 2) {
				int w = myView.getInt("Enter the width");
				int h = myView.getInt("Enter the height");
				String toShow = "";
				int i;
				for (i = 0; i < w; i++)
					toShow += "*";
				for (i = 0; i < h; i++)
					myView.say(toShow);
			}
		}
		
		myView.say("Demo End");
	}

}